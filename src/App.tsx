import Header from "./components/header/Header";
import Quiz from "./components/quiz/Quiz";

function App(): JSX.Element {
  return (
    <>
      <Header></Header>
      <Quiz></Quiz>
    </>
  );
}

export default App;
