export enum AnswerStateEnum {
  Init = "init",
  Answered = "answered",
  Correct = "correct",
  Wrong = "wrong",
  Selected = "selected",
  Skipped = "skipped",
}
