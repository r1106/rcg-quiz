import { IQuizTimer } from "../interfaces/quiz-timer.interface";

export const QUIZ_TIMER: IQuizTimer = {
  questionTime: 10000,
  answerSelectedTime: 1000,
  answerIsCorrectTime: 2000,
};
