export interface IQuestionComponentType {
  index: number;
  onSelectAnswer: (answer: string) => void;
  onTimeOut: () => void;
}
