export interface IQuestion {
  id: string;
  text: string;
  answers: string[];
}
