export interface IQuizTimerComponentType {
  timeout: number;
  onTimeOut: () => void;
  className: string;
}
