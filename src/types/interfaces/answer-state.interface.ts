export interface IAnswerState {
  selectedAnswer: string;
  isAnswerCorrect: boolean | null;
}
