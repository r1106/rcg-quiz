import { AnswerStateEnum } from "../enums/answer-states.enum";

export interface IAnswer {
  answers: string[];
  selectedAnswer: string;
  answerState: AnswerStateEnum;
  onSelect: (answer: string) => void;
}
