export interface IQuizTimer {
  questionTime: number;
  answerSelectedTime: number;
  answerIsCorrectTime: number;
}
