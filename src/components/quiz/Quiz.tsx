import { useCallback, useState } from "react";
import { QUESTIONS } from "../../types/consts/questions.const";
import { AnswerStateEnum } from "../../types/enums/answer-states.enum";
import Question from "../question/Question";
import Summary from "../summary/Summary";

function Quiz(): JSX.Element {
  const [userAnswers, setUserAnswers] = useState<string[]>([]);
  const activeQuestionIndex = userAnswers.length;
  const isQuizCommplete = activeQuestionIndex === QUESTIONS.length;

  const handleSelectAnswerCallback = useCallback(function handleSelectAnswer(
    selectedAnswer: string
  ): void {
    setUserAnswers((prevAnswers) => {
      return [...prevAnswers, selectedAnswer];
    });
  },
  []);

  const handleSkipAnswerCallback = useCallback(
    () => handleSelectAnswerCallback(AnswerStateEnum.Skipped.toString()),
    [handleSelectAnswerCallback]
  );

  if (isQuizCommplete) {
    return <Summary userAnswers={userAnswers}></Summary>;
  }

  return (
    <div id="quiz">
      <Question
        key={activeQuestionIndex}
        index={activeQuestionIndex}
        onSelectAnswer={handleSelectAnswerCallback}
        onTimeOut={handleSkipAnswerCallback}
      ></Question>
    </div>
  );
}

export default Quiz;
