import { useState } from "react";
import { QUESTIONS } from "../../types/consts/questions.const";
import { QUIZ_TIMER } from "../../types/consts/timer.const";
import { AnswerStateEnum } from "../../types/enums/answer-states.enum";
import { IAnswerState } from "../../types/interfaces/answer-state.interface";
import { IQuestionComponentType } from "../../types/interfaces/question-component-type.interface";
import Answers from "../answers/Answers";
import QuizTimer from "../quiz-timer/QuizTimer";

function Question({
  index,
  onSelectAnswer,
  onTimeOut,
}: IQuestionComponentType): JSX.Element {
  const [answer, setAnswer] = useState<IAnswerState>({
    selectedAnswer: "",
    isAnswerCorrect: null,
  });
  let timer = QUIZ_TIMER.questionTime;
  if (answer.selectedAnswer) {
    timer = QUIZ_TIMER.answerSelectedTime;
  }

  if (answer.isAnswerCorrect !== null) {
    timer = QUIZ_TIMER.answerIsCorrectTime;
  }

  function handleSelectAnswer(answer: string): void {
    setAnswer({
      selectedAnswer: answer,
      isAnswerCorrect: null,
    });

    setTimeout(() => {
      setAnswer({
        selectedAnswer: answer,
        isAnswerCorrect: QUESTIONS[index].answers[0] === answer,
      });

      setTimeout(() => {
        onSelectAnswer(answer);
      }, QUIZ_TIMER.answerIsCorrectTime);
    }, QUIZ_TIMER.answerSelectedTime);
  }

  let answerState = AnswerStateEnum.Init;
  if (answer.selectedAnswer && answer.isAnswerCorrect !== null) {
    answerState = answer.isAnswerCorrect
      ? AnswerStateEnum.Correct
      : AnswerStateEnum.Wrong;
  } else if (answer.selectedAnswer) {
    answerState = AnswerStateEnum.Answered;
  }

  return (
    <div id="question">
      <QuizTimer
        key={timer}
        timeout={timer}
        onTimeOut={onTimeOut}
        className={answerState.toString()}
      ></QuizTimer>
      <h2>{QUESTIONS[index].text}</h2>
      <Answers
        answers={QUESTIONS[index].answers}
        selectedAnswer={answer.selectedAnswer}
        answerState={answerState}
        onSelect={handleSelectAnswer}
      ></Answers>
    </div>
  );
}

export default Question;
