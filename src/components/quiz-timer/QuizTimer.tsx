import { useEffect, useState } from "react";
import { IQuizTimerComponentType } from "../../types/interfaces/quiz-timer-component-type.interface";

function QuizTimer({
  timeout,
  onTimeOut,
  className,
}: IQuizTimerComponentType): JSX.Element {
  const [remainingTime, setRemainingTime] = useState<number>(timeout);

  useEffect(() => {
    const timer = setTimeout(onTimeOut, timeout);

    return () => {
      clearTimeout(timer);
    };
  }, [timeout, onTimeOut]);

  useEffect(() => {
    const interval = setInterval(() => {
      setRemainingTime((prevTime) => prevTime - 100);
    }, 100);

    return () => {
      clearInterval(interval);
    };
  }, []);

  return (
    <progress
      id="question-time"
      max={timeout}
      value={remainingTime}
      className={className}
    ></progress>
  );
}

export default QuizTimer;
