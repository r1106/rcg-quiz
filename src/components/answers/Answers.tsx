import { useRef } from "react";
import { AnswerStateEnum } from "../../types/enums/answer-states.enum";
import { IAnswer } from "../../types/interfaces/answer.interface";

function Answers({
  answers,
  selectedAnswer,
  answerState,
  onSelect,
}: IAnswer): JSX.Element {
  const shuffeledAnswersRef = useRef<string[]>();

  if (!shuffeledAnswersRef.current) {
    shuffeledAnswersRef.current = [...answers].sort(() => Math.random() - 0.5);
  }

  return (
    <ul id="answers">
      {shuffeledAnswersRef.current.map((answer) => {
        const isSelected = selectedAnswer === answer;
        let cssClasses = "";
        if (answerState === AnswerStateEnum.Answered && isSelected) {
          cssClasses = AnswerStateEnum.Selected.toString();
        }

        if (answerState === AnswerStateEnum.Correct && isSelected) {
          cssClasses = AnswerStateEnum.Correct.toString();
        }

        if (answerState === AnswerStateEnum.Wrong && isSelected) {
          cssClasses = AnswerStateEnum.Wrong.toString();
        }

        return (
          <li key={answer} className="answer">
            <button
              onClick={() => onSelect(answer)}
              className={cssClasses}
              disabled={answerState !== AnswerStateEnum.Init}
            >
              {answer}
            </button>
          </li>
        );
      })}
    </ul>
  );
}

export default Answers;
