import quizCompleteImage from "../../assets/quiz-complete.png";
import { QUESTIONS } from "../../types/consts/questions.const";
import { AnswerStateEnum } from "../../types/enums/answer-states.enum";

export interface ISummary {
  userAnswers: string[];
}

function Summary({ userAnswers }: ISummary): JSX.Element {
  const skippedAnswers = userAnswers.filter(
    (answer) => answer === AnswerStateEnum.Skipped.toString()
  );
  const correctAnswers = userAnswers.filter(
    (answer, index) => answer === QUESTIONS[index].answers[0]
  );
  const skippedAnswersShare = Math.round(
    (skippedAnswers.length / userAnswers.length) * 100
  );
  const correctAnswersShare = Math.round(
    (correctAnswers.length / userAnswers.length) * 100
  );
  const wrongAnswersShare = 100 - skippedAnswersShare - correctAnswersShare;

  return (
    <div id="summary">
      <img src={quizCompleteImage} alt="Trophy icon"></img>
      <h2>Quiz Completed!</h2>
      <div id="summary-stats">
        <p>
          <span className="number">{skippedAnswersShare}%</span>
          <span className="text">skipped</span>
        </p>
        <p>
          <span className="number">{correctAnswersShare}%</span>
          <span className="text">answered correctly</span>
        </p>
        <p>
          <span className="number">{wrongAnswersShare}%</span>
          <span className="text">answered incorrectly</span>
        </p>
      </div>
      <ol>
        {userAnswers.map((answer, index) => {
          let cssClass = "user-answer";
          if (answer === AnswerStateEnum.Skipped.toString()) {
            cssClass += ` ${AnswerStateEnum.Skipped.toString()}`;
          } else if (answer === QUESTIONS[index].answers[0]) {
            cssClass += ` ${AnswerStateEnum.Correct.toString()}`;
          } else {
            cssClass += ` ${AnswerStateEnum.Wrong.toString()}`;
          }

          return (
            <li key={index}>
              <h3>{index + 1}</h3>
              <p className="question">{QUESTIONS[index].text}</p>
              <p className={cssClass}>
                {answer === AnswerStateEnum.Skipped.toString()
                  ? "Skipped"
                  : answer}
              </p>
            </li>
          );
        })}
      </ol>
    </div>
  );
}

export default Summary;
